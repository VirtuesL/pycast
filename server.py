from http import server
import mss, numpy as np
from cv2 import imencode
import time

class videoServerHandler(server.BaseHTTPRequestHandler):
  def do_HEAD(self):
    self.send_response(200)
    self.send_header("Content-type", "text/html")
    self.end_headers()

  def do_GET(self):
    if self.path.endswith(('0','1','2','3')):
      self.send_response(200),
      self.send_header('Content-type', 'multipart/x-mixed-replace; boundary=frame')
      self.end_headers()
      with mss.mss() as sct:
        while True:
          img = np.array(sct.grab(sct.monitors[int(self.path[-1])]))
          _, jpg = imencode('.jpeg', img)
          self.wfile.write(b"--frame\r\n")
          self.wfile.write(b'Content-Type: image/jpeg\r\n\r\n')
          self.wfile.write(jpg.tostring())
          self.wfile.write(b'\r\n')
    else:
      self.send_response(200)
      self.send_header('Content-type', 'text/html')
      self.end_headers()
      self.wfile.write(b'<html><head><title>Cams</title></head><body>')
      self.wfile.write(b'<img src="/1"/>')
      self.wfile.write(b'</body></html>')
            
if __name__ == '__main__':
  s = server.HTTPServer(('', 9000),videoServerHandler)

  s.serve_forever()