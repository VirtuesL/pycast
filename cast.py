import pychromecast as pc
import socket

locip = socket.gethostbyname(socket.gethostname())

casts = pc.get_chromecasts()

cast_names = [cast.device.friendly_name for cast in casts]
your_cast = cast_names[int(input('Choose Your cast '+str([str(i)+'. '+j for i,j in enumerate(cast_names)])+' '))]
cast = next(cc for cc in casts if cc.device.friendly_name == your_cast)

cc = pc.Chromecast(cast.host.split(',')[-1])
cc.wait()

mc = cc.media_controller
mc.stop()
mc.play_media('http://'+locip+':9000'+'/1','image/jpeg')
mc.play()
print(mc.status)
input()
mc.stop()